package com.atlassian.webdriver.utils;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Optional;
import java.util.function.Supplier;

public class BaseUrlSupplierForHost implements Supplier<String> {

    private final URI baseUrl;
    private final Supplier<Optional<String>> hostSupplier;

    public BaseUrlSupplierForHost(final String baseUrl, final Supplier<Optional<String>> hostSupplier) {
        this.baseUrl = URI.create(baseUrl);
        this.hostSupplier = hostSupplier;
    }

    @Override
    public String get() {
        return hostSupplier.get().map(this::createBaseUrlForHost).orElse(baseUrl).toString();
    }

    private URI createBaseUrlForHost(final String host) {
        try {
            return new URI(baseUrl.getScheme(), baseUrl.getUserInfo(), host, baseUrl.getPort(), baseUrl.getPath(),
                    baseUrl.getQuery(), baseUrl.getFragment());
        } catch (final URISyntaxException e) {
            throw new IllegalStateException("Could not supply new url for host:" + host + " and baseUrl:" + baseUrl);
        }
    }

}