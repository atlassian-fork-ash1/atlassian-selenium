package com.atlassian.webdriver.testing;

import com.atlassian.pageobjects.DefaultProductInstance;
import com.atlassian.pageobjects.MultiTenantTestedProduct;
import com.atlassian.pageobjects.ProductInstance;
import com.atlassian.pageobjects.TestedProductFactory;
import com.atlassian.webdriver.pageobjects.WebDriverTester;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.openqa.selenium.WebDriver;

import java.net.URI;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public abstract class BaseTestProductTest {

    private static final String DEFAULT_HOST = "atlassian.com";
    private static final String DEFAULT_BASE_URL = "http://" + DEFAULT_HOST;
    private static final String HOMEPAGE_URL_TEMPLATE = "http://%s/%s";

    protected final ProductInstance productInstance = new DefaultProductInstance(DEFAULT_BASE_URL, null, 80, "/");

    @Captor
    private ArgumentCaptor<String> urlCaptor;

    @Mock
    protected TestedProductFactory.TesterFactory<WebDriverTester> testerFactory;

    @Mock
    private WebDriverTester tester;

    @Mock
    protected WebDriver webDriver;

    @Before
    public void setup() {
        when(testerFactory.create()).thenReturn(tester);
        when(tester.getDriver()).thenReturn(webDriver);
    }

    protected abstract void goToHomePage();

    protected abstract String getHomePagePath();

    protected abstract MultiTenantTestedProduct getProduct();

    @Test
    public void testVisitsHomePageAtDefaultBaseUrl() {
        goToHomePage();
        verify(tester).gotoUrl(urlCaptor.capture());
        assertThat(urlCaptor.getValue(), is(homePageAtHost(DEFAULT_HOST)));
    }

    @Test
    public void testVisitsHomePageAtBaseUrlForHost() {
        final String expectedHost = "mytenant";

        getProduct().setHost(expectedHost);
        goToHomePage();

        verify(tester).gotoUrl(urlCaptor.capture());
        assertThat(urlCaptor.getValue(), is(homePageAtHost(expectedHost)));
        final String actualHost = URI.create(urlCaptor.getValue()).getHost();
        assertThat(actualHost, is(expectedHost));
    }

    private String homePageAtHost(final String host) {
        return String.format(HOMEPAGE_URL_TEMPLATE, host, getHomePagePath());
    }

}