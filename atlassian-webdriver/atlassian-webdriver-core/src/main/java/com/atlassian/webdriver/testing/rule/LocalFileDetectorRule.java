package com.atlassian.webdriver.testing.rule;

import com.atlassian.fugue.Suppliers;
import com.atlassian.webdriver.browsers.WebDriverBrowserAutoInstall;
import com.atlassian.webdriver.utils.WebDriverUtil;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.FileDetector;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.util.function.Supplier;

/**
 * Use this rule to allow uploading of local files in remote web drivers.
 *
 * @since 3.0
 */
public class LocalFileDetectorRule implements TestRule {

    private final Supplier<? extends WebDriver> webDriver;

    public LocalFileDetectorRule(WebDriver webDriver) {
        this(Suppliers.ofInstance(webDriver));
    }

    public LocalFileDetectorRule(Supplier<? extends WebDriver> webDriver) {
        this.webDriver = webDriver;
    }

    public LocalFileDetectorRule() {
        this.webDriver = WebDriverBrowserAutoInstall.driverSupplier();
    }

    @Override
    public Statement apply(final Statement base, Description description) {
        WebDriver driver = WebDriverUtil.getUnderlyingDriver(webDriver.get());
        if (!(driver instanceof RemoteWebDriver)) {
            return base;
        }

        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
                RemoteWebDriver remoteWebDriver = (RemoteWebDriver) driver;
                FileDetector previous = remoteWebDriver.getFileDetector();
                remoteWebDriver.setFileDetector(new LocalFileDetector());
                try {
                    base.evaluate();
                } finally {
                    remoteWebDriver.setFileDetector(previous);
                }
            }
        };
    }
}
