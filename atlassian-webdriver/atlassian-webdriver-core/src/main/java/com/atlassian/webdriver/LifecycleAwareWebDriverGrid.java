package com.atlassian.webdriver;

import com.atlassian.browsers.BrowserConfig;
import com.atlassian.webdriver.browsers.AutoInstallConfiguration;
import com.browserstack.local.Local;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.ref.WeakReference;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static com.atlassian.webdriver.utils.WebDriverUtil.getUnderlyingDriver;

/**
 * <p>
 * Simple lifecycle aware Webdriver helper that will setup auto browsers and then retrieve a driver from the factory.
 * Once the driver is running it will be re-used if the same browser property is retrieved again.
 * </p>
 *
 * <p>
 * When the runtime is shutdown it will handle cleaning up the browser. It also provides a way to manually quit all
 * the registered drivers/browsers via {@link #shutdown()}. When that method is called, the shutdown hooks for those
 * browser will be unregistered.
 * </p>
 *
 * @since 2.1
 */
public class LifecycleAwareWebDriverGrid
{
    private static final Logger log = LoggerFactory.getLogger(LifecycleAwareWebDriverGrid.class);
    private final static Map<String,WebDriverContext> drivers = new ConcurrentHashMap<>();

    private static final Map<String,WeakReference<Thread>> SHUTDOWN_HOOKS = new ConcurrentHashMap<>();

    private LifecycleAwareWebDriverGrid() {}

    /**
     * Retrieves the driver from the {@link WebDriverFactory}. If an instance
     * of the driver is already running then it will be re-used instead of
     * creating a new instance.
     *
     * @return an instance of the driver
     */
    public static WebDriverContext getDriverContext()
    {
        String browserProperty = WebDriverFactory.getBrowserProperty();
        WebDriverContext configuredContext = drivers.get(browserProperty);
        if (configuredContext != null)
        {
            return configuredContext;
        }

        final WebDriverContext context;
        if (BrowserstackWebDriverFactory.matches(browserProperty))
        {
            log.info("Loading browserstack driver");
            final Local bsLocal = new Local();

            try {
                context = new BrowserstackWebDriverFactory().getDriverContext(bsLocal);

                final Thread quitter = new Thread(() -> {
                    try {
                        if (bsLocal.isRunning()) {
                            bsLocal.stop();
                        }
                    } catch (Exception e) {
                        onQuitError(context.getDriver(), e);
                    }
                });
                SHUTDOWN_HOOKS.put(browserProperty, new WeakReference<>(quitter));
            } catch (Exception e) {
                throw new RuntimeException("Could not start Browserstack driver", e);
            }
        }
        else
        if (RemoteWebDriverFactory.matches(browserProperty))
        {
            log.info("Loading remote driver: " + browserProperty);
            context = RemoteWebDriverFactory.getDriverContext(browserProperty);
        }
        else
        {
            log.info("Loading local driver: " + browserProperty);
            BrowserConfig browserConfig = AutoInstallConfiguration.setupBrowser();
            context = WebDriverFactory.getDriverContext(browserConfig);
        }
        drivers.put(browserProperty, context);

        addShutdownHook(browserProperty, context.getDriver());
        return context;
    }

    /**
     * A manual shut down of the registered drivers. This basically resets the grid to a blank state. The shutdown hooks
     * for the drivers that have been closed are also removed.
     *
     */
    public static void shutdown()
    {
        for (Map.Entry<String,WebDriverContext> driver: drivers.entrySet())
        {
            quit(driver.getValue().getDriver());
            removeHook(driver);
        }
        drivers.clear();
        SHUTDOWN_HOOKS.clear();
    }

    private static void removeHook(Map.Entry<String, WebDriverContext> driver)
    {
        WeakReference<Thread> hookRef = SHUTDOWN_HOOKS.get(driver.getKey());
        final Thread hook = hookRef != null ? hookRef.get() : null;
        if (hook != null)
        {
            Runtime.getRuntime().removeShutdownHook(hook);
        }
    }

    private static void quit(WebDriver webDriver)
    {
        try
        {
            webDriver.quit();
        }
        catch (WebDriverException e)
        {
            onQuitError(webDriver, e);
        }
    }

    private static void addShutdownHook(final String browserProperty, final WebDriver driver) {
        final Thread quitter = new Thread()
        {
            @Override
            public void run()
            {
                log.debug("Running shut down hook for {}", driver);
                try
                {
                    drivers.remove(browserProperty);
                    log.info("Quitting {}", getUnderlyingDriver(driver));
                    driver.quit();
                    log.debug("Finished shutdown hook {}", this);
                }
                catch (NullPointerException e)
                {
                    // SELENIUM-247: suppress this error (but log it) since it's likely to be due to a harmless
                    // known issue where we're trying to clean up resources that the driver already cleaned up.
                    onQuitError(driver, e);
                }
                catch (WebDriverException e)
                {
                    onQuitError(driver, e);
                }
            }
        };
        SHUTDOWN_HOOKS.put(browserProperty, new WeakReference<>(quitter));
        Runtime.getRuntime().addShutdownHook(quitter);
    }


    private static void onQuitError(WebDriver webDriver, Exception e)
    {
        // there is no sense propagating the exception, and in 99 cases out of 100, the browser is already dead if an
        // exception happens
        log.warn("Exception when trying to quit driver {}: {}", webDriver, e.getMessage());
        log.debug("Exception when trying to quit driver - details", e);
    }

}
