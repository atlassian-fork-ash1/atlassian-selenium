package com.atlassian.webdriver.browsers;

import com.atlassian.webdriver.LifecycleAwareWebDriverGrid;
import org.openqa.selenium.WebDriver;
import org.slf4j.LoggerFactory;

import java.util.function.Supplier;

/**
 * Client that supports automatically installing the appropriate browser for the environment
 *
 */
public enum WebDriverBrowserAutoInstall
{
    INSTANCE;

    public WebDriver getDriver()
    {
        try {
            return LifecycleAwareWebDriverGrid.getDriverContext().getDriver();
        } catch (RuntimeException error) {
            LoggerFactory.getLogger(WebDriverBrowserAutoInstall.class).error("Unable to setup browser", error);
            throw error;
        }
    }

    public static Supplier<WebDriver> driverSupplier()
    {
        return INSTANCE::getDriver;
    }
}