package com.atlassian.webdriver.testing.rule;

import com.atlassian.webdriver.browsers.WebDriverBrowserAutoInstall;
import com.google.common.base.Suppliers;
import org.openqa.selenium.WebDriver;

import javax.annotation.Nonnull;
import java.util.function.Supplier;

import static java.util.Objects.requireNonNull;


/**
 * <p>
 * Support for rules that need a {@link org.openqa.selenium.WebDriver} instance.
 * </p>
 *
 * <p>
 * This class defines 'templates' for constructors to provide the WebDriver instance in a generic way. By default it
 * falls back to {@link com.atlassian.webdriver.LifecycleAwareWebDriverGrid}.
 * </p>
 *
 * @since 2.1
 */
public final class WebDriverSupport<WD extends WebDriver>
{
    public static WebDriverSupport<WebDriver> fromAutoInstall()
    {
        return new WebDriverSupport<>(WebDriverBrowserAutoInstall.driverSupplier());
    }

    public static <VE extends WebDriver> WebDriverSupport<VE> forSupplier(Supplier<? extends VE> driverSupplier)
    {
        return new WebDriverSupport<>(driverSupplier);
    }

    public static <VE extends WebDriver> WebDriverSupport<VE> forInstance(VE webDriver)
    {
        return new WebDriverSupport<>(() -> requireNonNull(webDriver, "webDriver"));
    }

    private final Supplier<? extends WD> driverSupplier;

    private WebDriverSupport(@Nonnull Supplier<? extends WD> driverSupplier)
    {
        this.driverSupplier = Suppliers.memoize(requireNonNull(driverSupplier, "driverSupplier")::get)::get;
    }

    @Nonnull
    public final WD getDriver()
    {
        return driverSupplier.get();
    }

}
