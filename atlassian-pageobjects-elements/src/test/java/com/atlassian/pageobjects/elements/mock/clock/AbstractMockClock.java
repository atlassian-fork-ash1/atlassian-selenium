package com.atlassian.pageobjects.elements.mock.clock;

import java.time.Clock;
import java.time.ZoneId;

import static java.time.ZoneOffset.UTC;

public abstract class AbstractMockClock extends Clock {

    @Override
    public ZoneId getZone() {
        return UTC;
    }

    @Override
    public Clock withZone(ZoneId zone) {
        throw new UnsupportedOperationException();
    }
}
