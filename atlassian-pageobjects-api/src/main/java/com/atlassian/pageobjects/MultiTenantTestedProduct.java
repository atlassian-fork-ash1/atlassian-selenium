package com.atlassian.pageobjects;

public interface MultiTenantTestedProduct<T extends Tester> extends TestedProduct<T> {

    /**
     * Optionally set the host to use for tenant-specific requests to the tested product,
     * otherwise the default URL shall be used.
     *
     * @param host for the required tenant, or null to reset to default
     */
    void setHost(String host);

    /**
     * Resets the host to the default for the tested product
     */
    default void resetToDefaultHost() {
        setHost(null);
    }

}
